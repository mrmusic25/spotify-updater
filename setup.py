from setuptools import setup, find_packages

setup(
    name="spotify-updater",
    version="0.0.1",
    license='GPLv3',
    author="Kyle Krattiger",
    author_email="",
    url="https://gitlab.com/mrmusic25/spotify-updater",
    description="iTunes to Spotify playlist synchronization program",
    long_description=open("README.md").read(),
    classifiers=[
        "Development Status :: 1 - Planning",
        "Environment :: Console",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent"
    ],
    packages=find_packages(include=['spotifyUpdater','spotifyUpdater.*']),
    install_requires=[
        'six>=1.15.0',
        'redis>=3.5.3',
        'requests>=2.25.0',
        'urllib3>=1.26.0',
        'spotipy',
        'pytest'
    ]
)