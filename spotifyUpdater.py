#!/usr/bin/python3

"""Main script for Spotify-updater program"""

### Imports

import pathlib
import argparse
import sqlite3
import spotipy
from configparser import ConfigParser
from Logger import Logger
from sys import stderr

### Globals and Defaults

homeDirectory = pathlib.Path.home()                          # User's home directory
configDirectory = homeDirectory.joinpath('.spotify-updater') # Directory used for project data, config, and logs
databaseFile = configDirectory.joinpath('song-database.db')  # Location of the SQLite database file
logFile = configDirectory.joinpath('spotify-updater.log')    # Location of the log file for Logger
log = Logger(str(logFile))                                   # Variable for the Logger class
configFile = configDirectory.joinpath('config.cfg')          # Config file used for storing user defaults and API keys
spotifyAPIkey = None                                         # API key from user's Spotify account. Program throws error if None at runtime
itunesXMLfile = homeDirectory.joinpath("Music").joinpath("iTunes").joinpath("iTunes Library.xml") # Location of the iTunes XML file
version = "0.0.1"                                            # Current version of program

### Functions

def changeConfigFolder(s: str):
    """Updates locations of all necessary files and folders when given a custom config folder location. Returns True on success, False otherwise."""
    global configDirectory, databaseFile, logFile, log, configFile
    if not s or s == "":
        print("ERROR: No config file given to changeConfigFolder()! Please fix and re-run!",file=stderr)
        return False

    # Put the directory in $HOME if the given directory is not absolute
    if pathlib.Path(s).is_absolute():
        configDirectory = pathlib.Path(s)
    else:
        configDirectory = homeDirectory.joinpath(s)

    # Make sure we can use the directories
    try:
        pathlib.Path(configDirectory).mkdir(parents=True,exist_ok=True)
    except:
        print("ERROR: Could not create directories for " + str(configDirectory) + "! Does current user have permissions? Please fix and re-run!",file=stderr)
        return False

    # Do not check for folder validity here, that will be handled later
    databaseFile = configDirectory.joinpath('song-database.db')
    logFile = configDirectory.joinpath('soptify-updater.log')
    configFile = configDirectory.joinpath('config.cfg')  # This could cause issues, but should be fine since function will be called before possible custom config file location.
    log = Logger(s)

    return True

def loadConfig():
    """Loads config from global configFile. Returns True on success, False otherwise."""
    global configDirectory, spotifyAPIkey, itunesXMLfile
    if not pathlib.Path(configFile).exists():
        log.log("Config file " + str(configFile) + " does not exist, nothing to import! Attempting to continue...",2)
        return False

    conf = ConfigParser()
    conf.read(str(configFile))
    vars = conf.options('Global')

    if "configDirectory" in vars:
        if not changeConfigFolder(conf['Global']['configDirectory']):
            print("ERROR: Could not set config directory from stored value: " + str(conf["Global"]['configDirectory']) + "! Attempting to continue...",file=stderr)
    if "spotifyAPIkey" in vars:
        spotifyAPIkey = conf["Global"]['spotifyAPIkey']
    if "itunesXMLfile" in vars:
        itunesXMLfile = pathlib.Path(conf["Global"]['itunesXMLfile'])

    return True

def saveConfig():
    """Saves config to global configFile. Returns True on success, False otherwise."""
    conf = ConfigParser()
    conf.add_section('Global')
    conf.set('Global','configDirectory',str(configDirectory))
    conf.set('Global','spotifyAPIkey',str(spotifyAPIkey))
    conf.set('Global','itunesXMLfile',str(itunesXMLfile))

    try:
        with open(str(configFile),'w') as f:
            conf.write(f)
            f.close()
    except:
        print("ERROR: Could not write to config file " + str(configFile) + "! Does current user have permission? Attempting to continue...",file=stderr)
        return False
    
    return True

### Main

if __name__ == "__main__":

    # Setup the argument parser
    parser = argparse.ArgumentParser(description="A program to convert iTunes playlists to Spotify using the iTunes' XML file and the Spotify API")
    parser.add_argument('-c','--config-directory',dest='configdir',help="The directory where configuration, logs, and databases are kept. Default is $HOME/.spotify-updates",default=None,type=str)
    parser.add_argument('-d','--database-file',dest='dbfile',help="Name of the file where the SQLite database is kept. If not given a full path, will be stored in the config directory.",default=None,type=str)
    parser.add_argument('-f','--config-file',dest='conffile',help="Configurations file where settings will be kept. If full path is not given, will be stored in config directory.",default=None,type=str)
    parser.add_argument('-a','--api-key',dest='apikey',help="Changes API key used for connecting to Spotify.",default=None,type=str)
    parser.add_argument('-i','--itunes-file',dest='itunesxml',help="Location of the iTunes XML file.",default=None,type=str)
    parser.add_argument('-v','--verbose',dest='verbose',help="Enables additional logging of informational messages to console.",default=None,action='store_true')
    parser.add_argument('-vv','--vverbose',dest='vverbose',help="Enables debug logging for diagnostics.",default=None,action='store_true')

    try:
        args = parser.parse_args()
    except argparse.ArgumentError:
        print('ERROR: Could not parse arguments! Please fix and re-run!')
        exit(1)

    # Load the config first so we can check to see if default should be overwritten. Make sure we don't need to change directory/file first though.
    if args.configdir:
        if not changeConfigFolder(args.configdir):
            print("ERROR: Could not update the config directory! Please fix and re-run! Exiting...",file=stderr)
            exit(1)
        else:
            log.log("Successfully changed the config directory to " + str(configDirectory))

    if args.conffile:
        if pathlib.Path(args.conffile).is_absolute():
            configFile = pathlib.Path(args.conffile)
        else:
            configFile = configDirectory.joinpath(args.conffile)
        log.log("Config file is now set to: " + str(configFile))

    if not loadConfig():
        log.log("Config could not be loaded! Attempting to continue...",2)

    # Load the rest of the arguments
    if args.dbfile:
        if pathlib.Path(args.dbfile).is_absolute():
            databaseFile = pathlib.Path(args.dbfile)
        else:
            databaseFile = configDirectory.joinpath(args.dbfile)
        log.log("Database file now set to: " + str(args.dbfile))

    if args.apikey:
        spotifyAPIkey = args.apikey # TODO: Verify key is valid? Correct length, is hex, etc. Possible confirmation test?
        log.log("Setting the Spotify API key to: " + spotifyAPIkey)

    if args.itunesxml:
        if not pathlib.Path(args.itunesxml).is_absolute():
            log.log("Given iTunes XML file is not an absolute path! Program will continue, but this may cause issues!",2)
        itunesXMLfile = pathlib.Path(args.itunesxml)
        log.log("Setting the iTunes XML file to: " + str(itunesXMLfile))

    if args.verbose:
        log.setPrintLevel(0)
        log.log("Verbose logging enabled!")

    if args.vverbose:
        log.debugMode()
        log.log("Enabled debugging verbose logging to both console and log!",0)

    # Everything is set, save the config
    if not saveConfig():
        log.log("Could not save config! Does user have permissions for " + str(configDirectory.parent) + "? Continuing without saving, some settings may be lost!",3)
    else:
        log.log("Successfully save the config! Moving on...")

    # Almost ready to start. First, make sure iTunes file is valid and that we have an API key
    if not spotifyAPIkey or spotifyAPIkey == "":
        log.log("Spotify API key is not set!")

    exit(0)