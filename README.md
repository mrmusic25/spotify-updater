# spotify-updater
Script, written in python3, that imports the XML file from iTunes and matches them to existing songs in Spotify to re-create playlists using their web API.
All Spotify ID matches will be saved to a local SQL database to limit APi usage and speed up the script.

## Installation
WIP. Instructions will be added as a working version approaches release.

## Usage
`python3 spotifyConverter.py [options] <iTunes Library.xml>`

## License
Licensed with GNU General Public License v3. See [LICENSE](https://gitlab.com/mrmusic25/spotify-updater/-/blob/main/LICENSE) file for more info.c